package com.nmb

import io.micronaut.context.ApplicationContext
import io.micronaut.http.client.HttpClient
import io.micronaut.runtime.server.EmbeddedServer
import io.micronaut.test.annotation.MicronautTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

@MicronautTest
internal class FormulariosResponseTest {
    @Test
    fun arrastreGruaTest() {
        var embeddedServer: EmbeddedServer = ApplicationContext.run(EmbeddedServer::class.java)
        var client: HttpClient = HttpClient.create(embeddedServer.url)
        var rsp: String = client.toBlocking().retrieve("/formularios/arrastre_grua")

        Assertions.assertNotEquals(
                rsp,
                ""
        )
        Assertions.assertNotNull(
                rsp
        )
    }

    @Test
    fun cambioLlantaTest() {
        var embeddedServer: EmbeddedServer = ApplicationContext.run(EmbeddedServer::class.java)
        var client: HttpClient = HttpClient.create(embeddedServer.url)
        var rsp: String = client.toBlocking().retrieve("/formularios/cambio_llanta")

        Assertions.assertNotEquals(
                rsp,
                ""
        )
        Assertions.assertNotNull(
                rsp
        )
    }

    @Test
    fun conductorElegidoTest() {
        var embeddedServer: EmbeddedServer = ApplicationContext.run(EmbeddedServer::class.java)
        var client: HttpClient = HttpClient.create(embeddedServer.url)
        var rsp: String = client.toBlocking().retrieve("/formularios/conductor_elegido")

        Assertions.assertNotEquals(
                rsp,
                ""
        )
        Assertions.assertNotNull(
                rsp
        )
    }
}