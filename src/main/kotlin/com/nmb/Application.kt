package com.nmb

import io.micronaut.runtime.Micronaut

object Application {

    @JvmStatic
    fun main(args: Array<String>) {
        Micronaut.build()
                .packages("com.nmb")
                .mainClass(Application.javaClass)
                .start()
    }
}