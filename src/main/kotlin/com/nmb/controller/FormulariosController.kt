package com.nmb.controller

import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Produces
import io.micronaut.http.MediaType
import java.io.File

@Controller("/formularios")
class FormulariosController {

    // Responde el formulario para arrastre de grua
    @Get("/arrastre_grua")
    @Produces(MediaType.APPLICATION_JSON)
    fun arrastreGrua(): String {
        val jsonString: String = File("./jsonFiles/arrastre_grua.json").readText(Charsets.UTF_8)
        return jsonString
    }

    // Responde el formulario para paso corriente
    @Get("/paso_corriente")
    @Produces(MediaType.APPLICATION_JSON)
    fun pasoCorriente(): String {
        val jsonString: String = File("./jsonFiles/paso_corriente.json").readText(Charsets.UTF_8)
        return jsonString
    }

    // Responde el formulario para cambio llanta
    @Get("/cambio_llanta")
    @Produces(MediaType.APPLICATION_JSON)
    fun cambioLlanta(): String {
        val jsonString: String = File("./jsonFiles/cambio_llanta.json").readText(Charsets.UTF_8)
        return jsonString
    }

    // Responde el formulario para suministro de gasolina
    @Get("/suministro_gasolina")
    @Produces(MediaType.APPLICATION_JSON)
    fun suministroGasolina(): String {
        val jsonString: String = File("./jsonFiles/suministro_gasolina.json").readText(Charsets.UTF_8)
        return jsonString
    }

    // Responde el formulario para conductor elegido
    @Get("/conductor_elegido")
    @Produces(MediaType.APPLICATION_JSON)
    fun conductorElegido(): String {
        val jsonString: String = File("./jsonFiles/conductor_elegido.json").readText(Charsets.UTF_8)
        return jsonString
    }

    // Responde el formulario para cerrajeria de auto
    @Get("/cerrajeria_auto")
    @Produces(MediaType.APPLICATION_JSON)
    fun cerrajeriaAuto(): String {
        val jsonString: String = File("./jsonFiles/cerrajeria_auto.json").readText(Charsets.UTF_8)
        return jsonString
    }

    // Responde el formulario para grua averiada
    @Get("/grua_averia")
    @Produces(MediaType.APPLICATION_JSON)
    fun gruaAveria(): String {
        val jsonString: String = File("./jsonFiles/grua_averia.json").readText(Charsets.UTF_8)
        return jsonString
    }

    // Responde el formulario para grua accidente
    @Get("/grua_accidente")
    @Produces(MediaType.APPLICATION_JSON)
    fun gruaAccidente(): String {
        val jsonString: String = File("./jsonFiles/grua_accidente.json").readText(Charsets.UTF_8)
        return jsonString
    }
}