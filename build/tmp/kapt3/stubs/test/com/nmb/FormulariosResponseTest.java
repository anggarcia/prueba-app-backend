package com.nmb;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0001\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0007J\b\u0010\u0005\u001a\u00020\u0004H\u0007J\b\u0010\u0006\u001a\u00020\u0004H\u0007\u00a8\u0006\u0007"}, d2 = {"Lcom/nmb/FormulariosResponseTest;", "", "()V", "arrastreGruaTest", "", "cambioLlantaTest", "conductorElegidoTest", "formularios"})
@io.micronaut.test.annotation.MicronautTest()
public final class FormulariosResponseTest {
    
    @org.junit.jupiter.api.Test()
    public final void arrastreGruaTest() {
    }
    
    @org.junit.jupiter.api.Test()
    public final void cambioLlantaTest() {
    }
    
    @org.junit.jupiter.api.Test()
    public final void conductorElegidoTest() {
    }
    
    public FormulariosResponseTest() {
        super();
    }
}