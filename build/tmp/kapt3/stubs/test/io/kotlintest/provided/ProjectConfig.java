package io.kotlintest.provided;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004H\u0016J\u000e\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004H\u0016\u00a8\u0006\u0007"}, d2 = {"Lio/kotlintest/provided/ProjectConfig;", "Lio/kotlintest/AbstractProjectConfig;", "()V", "extensions", "", "Lio/micronaut/test/extensions/kotlintest/MicronautKotlinTestExtension;", "listeners", "formularios"})
public final class ProjectConfig extends io.kotlintest.AbstractProjectConfig {
    public static final io.kotlintest.provided.ProjectConfig INSTANCE = null;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.util.List<io.micronaut.test.extensions.kotlintest.MicronautKotlinTestExtension> listeners() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.util.List<io.micronaut.test.extensions.kotlintest.MicronautKotlinTestExtension> extensions() {
        return null;
    }
    
    private ProjectConfig() {
        super();
    }
}